package models

import "gopkg.in/mgo.v2/bson"

// Represents a movie, we use the bson keyword to tell the mgo griver how to name
// the properties in the mongodb document
type Movie struct {
	ID          bson.ObjectId `bson:"_id" json:"id"`
	Name        string        `bson:"name" json:"name"`
	CoverImage  string        `bson:"cover_image" json:"cover_image"`
	Description string        `bson:"description" json:"description"`
}
