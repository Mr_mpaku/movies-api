// Controllers for each API endpoint
package main

import (
	"encoding/json"
	"log"
	"net/http"

	"gopkg.in/mgo.v2/bson"

	"github.com/gorilla/mux"
	. "github.com/mr-mpaku/rest-tutorials/movies-api/config"
	. "github.com/mr-mpaku/rest-tutorials/movies-api/dao"
	. "github.com/mr-mpaku/rest-tutorials/movies-api/models"
)

// ---- not sure what they do --------- Explain after evaluation
var config = Config{}
var dao = MoviesDAO{}

// AllMoviesEndPoint : uses the "FindAll()" method of DAO to fetch the list of all the movies in the database: GET METHOD
func AllMoviesEndPoint(w http.ResponseWriter, r *http.Request) {

	movies, err := dao.FindAll()

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, movies)
}

// FindMovieEndPoint : uses the "mux" library to get the parameters that the users passed in with the request: GET METHOD
func FindMovieEndPoint(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	movie, err := dao.FindById(params["id"])

	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid Movie ID")
		return
	}

	respondWithJson(w, http.StatusOK, movie)
}

// CreateMovieEndPoint : decodes the request body into a movie object, assigns it an ID, and then uses the DAO Insert method to create a movie in the database: POST METHOD
func CreateMovieEndPoint(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()
	var movie Movie

	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	movie.ID = bson.NewObjectId()

	if err := dao.Insert(movie); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusCreated, movie)
}

// UpdateMovieEndPoint : updates the details of an existing movie: PUT METHOD
func UpdateMovieEndPoint(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()
	var movie Movie

	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}

	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

// DeleteMovieEndPoint : deletes an existing movie DELETE METHOD
func DeleteMovieEndPoint(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()
	var movie Movie

	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	if err := dao.Delete(movie); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJson(w, http.StatusOK, map[string]string{"result": "success"})
}

//
func respondWithError(w http.ResponseWriter, code int, msg string) {

	respondWithJson(w, code, map[string]string{"error": msg})

}

//
func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {

	response, _ := json.Marshal(payload)
	w.Header().Set("Contet-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)

}

// Parse the configuration file 'config.toml', and establish a connection to the DB
func init() {

	config.Read()
	dao.Server = config.Server
	dao.Database = config.Database
	dao.Connect()

}

// Define the HTTP routes
func main() {
	r := mux.NewRouter()
	r.HandleFunc("/movies", AllMoviesEndPoint).Methods("GET")
	r.HandleFunc("/movies", CreateMovieEndPoint).Methods("POST")
	r.HandleFunc("/movies", UpdateMovieEndPoint).Methods("PUT")
	r.HandleFunc("/movies", DeleteMovieEndPoint).Methods("DELETE")
	r.HandleFunc("/movies/{id}", FindMovieEndPoint).Methods("GET")

	// Checking if PORT 3000 is open
	if err := http.ListenAndServe(":8000", r); err != nil {
		log.Fatal(err)
	}
}
