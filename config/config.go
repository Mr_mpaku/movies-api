package config

import (
	"log"

	"github.com/BurntSushi/toml"
)

// Config : Represents the database server and credentials
type Config struct {
	Server   string
	Database string
}

// Read : reads and parses the configuration file
func (c *Config) Read() {
	if _, err := toml.DecodeFile("config.toml", &c); err != nil {
		log.Fatal(err)
	}
}
